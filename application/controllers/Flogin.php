<?php

require_once (APPPATH."vendor/Falf/Sql.php");
class Flogin extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('front');
    }

    function index()
    {
        if (http_method('post')) {
            $do = http_post('do');
            if ($do && $do != 'index' && method_exists($this, $do)) {
                call_user_func([$this, $do]);
            }
        } else {
            //$v = new \vendor\Sql();
            //$data = $v->sql('SELECT * FROM phppos_employees WHERE 1;')->findMany();

            $this->load->view("front/flogin", []);
        }
    }

    private function login(){
        require_once (APPPATH."vendor/Falf\Users\Account\Login.php");

        $login = new \Falf\Users\Account\Login();
        $login->logUser()->by(['username','email','mobile'])->provider('email')
            ->onSuccess(function($user){
                redirect('fhome');
            })->onFail(function(){
                redirect('flogin');
            })->run();
    }

}
?>