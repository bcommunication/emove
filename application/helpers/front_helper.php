<?php

define('STOP_EXECUTION', 11);
define('CONTINUE_EXECUTION', 12);

//http functions
function http_method($method = NULL)
{
    $meth = $_SERVER['REQUEST_METHOD'];
    $m = $meth?strtolower($meth):'any';
    unset($meth);
    if($method)
    {
        return strtolower($m) === strtolower($method);
    }
    return strtolower($m);
}

function http_get($key = NULL)
{
    $g = filter_input_array(INPUT_GET);
    if($key)
    {
        return recursive_array_get($key,$g );
    }
    return filter_input_array(INPUT_GET);
}

function http_post($key = NULL)
{
    $p = filter_input_array(INPUT_POST);
    if($key)
    {
        return recursive_array_get($key, $p);
    }
    return (array)$p;
}

//session functions
function start_session()
{
    if(isset($_COOKIE[session_name()])&&!$_COOKIE[session_name()])
    {
        setcookie(session_name(), NULL,-1);
        return;
    }
    if(session_status() === PHP_SESSION_NONE)
    {
        session_start();
    }
}

function session_get($key)
{
    start_session();
    return recursive_array_get($key, $_SESSION);
}
function session_set($key,$value)
{
    start_session();
    recursive_array_set($key, $value, $_SESSION);
}


function session_delete($key)
{
    start_session();
    recursive_array_set($key, NULL, $_SESSION);
}

function session_has($key)
{
    start_session();
    return recursive_array_has($key, $_SESSION);
}

//array functions
function recursive_array_set($key,$value,&$array)
{
    $k = explode('.', $key);
    $target = &$array;
    foreach ($k as $v)
    {
        if(!isset($target[$v]))
        {
            $target[$v] = [];
        }
        $target = &$target[$v];
    }
    $target = $value;
}

function recursive_array_get($key,&$array)
{
    $k = explode('.', $key);
    $target = &$array;
    foreach ($k as $v)
    {
        if(!isset($target[$v]))
        {
            return FALSE;
        }
        $target = &$target[$v];
    }
    return $target;
}

function recursive_array_has($key,&$array)
{
    $k = explode('.', $key);
    $target = &$array;
    foreach ($k as $v)
    {
        if(!isset($target[$v])||  is_null($target[$v]))
        {
            return FALSE;
        }
        $target = &$target[$v];
    }
    return TRUE;
}
