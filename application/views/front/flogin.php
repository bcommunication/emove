<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view("front/partials/head"); ?>
</head>
<style>
    @media (max-width: 991px) {
        .owl-carousel.home-slider {
            /*height: 578px;*/
        }
    }
    @media (min-width: 992px) {
        .owl-carousel.home-slider {
            /*height: 737px;*/
        }
    }
</style>
<body>
<?php $this->load->view("front/partials/navbar"); ?>

<section class="home-slider owl-carousel img">

</section>

<section class="container">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4" style="margin-top: 9%;">
            <form class="panel panel-info" method="post" name="form-login" action="">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="ico-lock3 mr5"></i> D&Eacute;J&Agrave; CLIENT ?
                    </h3>
                </div>
                <div class="panel-body">
                    <?php if(isset($response->error)): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <span class="semibold">
                                        <?php if($response->error == 'max_try'): ?>
                                            vous avez essayé trop de fois
                                        <?php else: ?>
                                            mot de passe ou nom d'utilisateur incorrect
                                        <?php endif; ?>
                                    </span>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <div class="form-stack has-icon pull-left">
                            <input name="email" type="text" class="form-control input-lg" placeholder="Email ou nom d'utilisateur"  value="" data-parsley-errors-container="#error-container" data-parsley-error-message="Nom d'utilisateur / email requis" data-parsley-required>
                            <i class="ico-user2 form-control-icon"></i>
                        </div>
                        <div class="form-stack has-icon pull-left">
                            <input name="password" type="password" class="form-control input-lg" placeholder="Mot de passe" data-parsley-errors-container="#error-container" data-parsley-error-message="Mot de passe requis" data-parsley-required>
                            <i class="ico-lock2 form-control-icon"></i>
                        </div>
                    </div>
                    <div id="error-container"class="mb15"></div>
                    <div class="form-group nm">
                        <input name="do" value="login" type="hidden">
                        <button type="submit" class="btn btn-block btn-success"><span class="semibold">Se connecter</span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<footer class="section-footer fixed-bottom img">
    <?php //view_part('front.partials.footer',['info'=>$info]); ?>
</footer>
<script>
    $(document).ready(() => {
        var $form    = $('form[name=form-login]');
        $form.on('click', 'button[type=submit]', function (e) {
            var $this = $(this);
            if (!$form.parsley().validate())
            {
                $form
                    .removeClass('animation animating shake')
                    .addClass('animation animating shake')
                    .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                        $(this).removeClass('animation animating shake');
                    });
            }
        });
    });

</script>
</body>
</html>