<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("front/partials/head"); ?>
</head>
<style>
    @media (max-width: 991px) {
        .owl-carousel.home-slider {
            /*height: 578px;*/
        }
    }
    @media (min-width: 992px) {
        .owl-carousel.home-slider {
            /*height: 737px;*/
        }
    }
</style>
<body>
	<?php $this->load->view("front/partials/navbar"); ?>
	<?php $this->load->view("front/partials/navbar3"); ?>

	<section class="home-slider owl-carousel img">

	</section>
	<footer class="section-footer fixed-bottom img">
		<?php //view_part('front.partials.footer',['info'=>$info]); ?>
	</footer>
	<script>
		$('form[name="bookTable"]').attr('action', document.URL);
		if ($('#date')[0].type != 'date') {
			$('#date').datepicker({
				format: "yyyy-mm-dd"
			});
		}
		$(document).ready(() => {
			var url = window.location.href;
			if (url.indexOf("#") > 0) {
				var activeTab = url.substring(url.indexOf("#") + 1);
				$('.nav[role="tablist"] a[href="#' + activeTab + '"]').tab('show');
				var position = $("#menu").offset().top - $(".navbar").height();
				$("html, body").animate({
					scrollTop: position
				}, 1000);
			}
		});
		// Events carousel
		$(".events-carousel").owlCarousel({
			autoplay: true,
			dots: true,
			loop: true,
			items: 1,
			autoplayTimeout: 10000
		});
	</script>
</body>
</html>