<?php
require_once (APPPATH."vendor\Falf\Users\Acl.php");
require_once (APPPATH."vendor\Falf\Users\Account\Logout.php");

$acl = new \vendor\Falf\Users\Acl();
function logout($token)
{
    $l = new \Falf\Users\Account\Logout();
    $l->token($token)->onFail(function(){

    })->onSuccess(function(){
        redirect('fhome');
    })->run();
}
if(http_get('logout'))
    logout(session_get('users')['login_token']);
?>
<nav class="navbar navbar-expand-lg" id="navbar_first">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#section-nav" aria-controls="section-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"> Menu</span>
        </button>
        <div class="collapse navbar-collapse" id="section-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item <?php echo $this->uri->segment(1) == 'fhome' ? 'active' : '' ?>"><a href="<?= site_url('fhome') ?>" class="nav-link"><i class="fa fa-search"></i> Localisation</a></li>
                <?php if ($acl::getInstance()->isGuest()): ?>
                    <li class="nav-item <?php echo $this->uri->segment(1) == 'flogin' ? 'active' : '' ?>"><a href="<?= site_url('flogin') ?>" class="nav-link"><i class="fa fa-user"></i> Mon compte</a></li>
                <?php else: ?>
                <li class="dropdown">
                    <a style="text-align: center" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"> Mon compte</i></a>
                    <ul class="dropdown-menu">
                        <li><a href="">Paramétres</a></li>
                        <li class="logout"><a href="<?= site_url('fhome').'?logout=true' ?>">Déconnexion</a></li>
                    </ul>
                </li>
                <?php endif; ?>
                <li class="nav-item"><a href="" class="nav-link"><i class="fa fa-shopping-basket"></i> Mon panier(0)</a></li>
            </ul>
                <div class="hidden-md hidden-lg clearfix"></div>
            </ul>
        </div>
    </div>
</nav>


