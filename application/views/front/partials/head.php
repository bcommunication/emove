	<title>emove</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Restaurant Chinois ">
    <meta name="keywords" content="Restaurant Aline tahiti, Snack restaurant chinois">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Nothing+You+Could+Do" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url() ?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/vendor/open-iconic-bootstrap.css/open-iconic-bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/vendor/animate/animate_2015.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/vendor/owl.carousel/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/vendor/owl.carousel/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/vendor/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/vendor/aos/css/aos.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/vendor/ionic.css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/vendor/bootstrap/css/bootstrap-datepicker.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/vendor/jquery/css/jquery.timepicker.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/vendor/flaticon.css/flaticon.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/vendor/icomoon.css/icomoon.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/vendor/css/maher.css?<?= uniqid(); ?>">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/vendor/css/navbar3.css?<?= uniqid(); ?>">
    <script src="<?php echo base_url() ?>/assets/vendor/jquery/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/vendor/jquery/js/jquery-migrate-3.0.1.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/vendor/popper/js/popper.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/vendor/jquery/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/vendor/jquery/js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/vendor/jquery/js/jquery.stellar.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/vendor/jquery/js/jquery.timepicker.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/vendor/jquery/js/jquery.animateNumber.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/vendor/jquery/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/vendor/owl.carousel/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/vendor/aos/js/aos.js"></script>
    <script src="<?php echo base_url() ?>/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/vendor/scrollax/scrollax.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/vendor/php-validate-form/validate.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>/assets/vendor/parsley/js/parsley.js"></script>
